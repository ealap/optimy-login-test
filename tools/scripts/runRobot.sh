#!/usr/bin/env bash

stty -echoctl
set -Eeuo pipefail
trap 'exit $?' SIGINT SIGTERM ERR
trap 'fd_cleanup "${?}"' EXIT

gv_browser="chrome"
gv_tags="all"
gv_path="src/tests"

function fd_cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    unset gv_browser gv_path gv_tags
}

function fd_parse_opt() {
    while :; do {
        local v_opt="${1:-?}"
        case "${v_opt}" in
            --browser )
                gv_browser="${2}"
                shift
                ;;
            --tags )
                gv_tags="${2}"
                shift
                ;;
            --path )
                gv_path="${2}"
                shift
                ;;
            * ) break ;;
        esac
        shift
    }; done
}

function main() {
    declare -a robot_opts
    robot_opts+=(--outputdir "logs" --timestampoutputs)
    robot_opts+=(--variable "BROWSER:${gv_browser}" --variable "USER:user${RANDOM}@testing.com")
    robot_opts+=(--include "${gv_tags}" "${gv_path}")

    # Execute robot script
    robot "${robot_opts[@]}"
}

fd_parse_opt "${@}"
main