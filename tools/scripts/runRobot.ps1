#!/usr/bin/env pwsh

Param(
    [Parameter(Mandatory = $False)]
    [string[]]$Browser = "chrome",
    [Parameter(Mandatory = $False)]
    [string[]]$Tags = "all",
    [Parameter(Mandatory = $False)]
    [string[]]$Path = "src/tests"
)

function main {
    robot --outputdir "logs" --timestampoutputs `
    --variable "BROWSER:$Browser" --variable "USER:user$(Get-Random)@testing.com" `
    --include "$Tags" "$Path"
}

main