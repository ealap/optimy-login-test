# Tests for Optimy App Login page

A repository of automation scripts for testing the [Optimy Login page](https://login.optimyapp.com/).

## Test Envinronment Setup
### Requirements:
- Python 3.6 or newer
- VirtualEnv 20.0.0 or newer (optional)
- RobotFramework 3.1.1 or newer
- RobotFramework/SeleniumLibrary 3.3.1 or newer

#### Virtualenv Setup (Optional)
a. Create a python virtualenv<br>
``` bash
python3 -m virtualenv "YOUR-VIRTUALENV-NAME"
```
b. Activate the environment
##### For Windows CMD or Powershell:
``` PowerShell
"\path\to\virtualenv_name\bin\activate"
```
##### For Linux shell:
``` bash
source "/path/to/virtualenv_name/bin/activate"
```

#### Clone test repository
``` bash
git clone "https://gitlab.com/ealap/optimy-login-test.git"
```

#### Install Dependencies
``` bash
cd optimy-login-test            # replace with custom path to repo if necessary
pip install -r requirements.txt # install packages listed in requirements.txt
```

#### Webdriver setup
##### For Windows:
``` PowerShell
webdrivermanager chrome firefox ie --linkpath "/path/to/virtualenv/folder/Scripts"
```
##### For Linux:
``` bash
webdrivermanager chrome firefox --linkpath "/path/to/virtualenv/folder/bin"
```
> Note: IE webdriver is only available for Windows env

## Test Execution
### Selecting tests
##### Per test:
``` bash
robot "src/tests/Login/invalid_login_handling.robot"
```
##### Per tags (login, recovery, all):
``` bash
robot --include "recovery"
```
##### All testcases:
``` bash
robot --include "all" "src/tests"
```
### Selecting supported browser (chrome, firefox, ie, headlesschrome, headlessfirefox)
``` bash
robot --variable BROWSER:headlessfirefox --include "all" "src/tests"
```
### Ensure test will not fail due to account lock feature
##### For Windows Powershell:
``` PowerShell
robot --variable USER:$(Get-Random) --include "src/tests/Login/invalid_login_handling.robot"
```
##### For Linux shell:
``` bash
robot --variable USER:${RANDOM} --include "src/tests/Login/invalid_login_handling.robot"
```
### Using robot-runner script
##### For Windows PowerShell:
``` PowerShell
.\tools\scripts\runRobot.ps1 -Browser "firefox" -Tags "all" -Path "src/tests"
```
##### For Linux:
``` bash
./tools/scripts/runRobot.sh --browser "headlesschrome" --tags "all" --path "src/tests"
```
<sup>2021 Emmanuel Alap</sup>