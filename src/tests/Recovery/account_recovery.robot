*** Settings ***
Documentation  Testcase for Optimy App Login page.
...
...            Author: ealap
...            MIT License. Copyright 2021 Emmanuel Alap
Default Tags   all  optimy-tests  recovery

Suite Setup     Open Browser To Recovery Page
Suite Teardown  Close Browser
Resource        ../../common/keywords.resource

*** Variables ***
${BUTTON_RECOVERY}        default:Recover my access
${FIELD_RECOVERY_EMAIL}   xpath://*[@id="tab-recovery-ask"]/form/div/input
${HELP_EMAIL}             xpath://*[@id="tab-recovery-ask"]/form/div/span
${LINK_LOST_PASSWORD}     default:Lost password?

*** Test Cases ***
Invalid Email Address Format Should Not Be Accepted
    Input Recovery Email                  not_an_email
    Element Should Contain                ${HELP_EMAIL}            ${MESSAGE_INVALID}

Recovery With Empty Email Address Should Fail
    Enter Email And Submit For Recovery   ${EMPTY}
    Element Should Contain                ${HELP_EMAIL}            ${MESSAGE_REQUIRED}

*** Keywords ***
Input Recovery Email
    [Arguments]                           ${user_address}
    Wait Until Element Is Visible         ${FIELD_RECOVERY_EMAIL}
    Input Text                            ${FIELD_RECOVERY_EMAIL}  ${user_address}

Open Browser To Recovery Page
    Setup Test
    Click Link                            ${LINK_LOST_PASSWORD}

Enter Email And Submit For Recovery
    [Arguments]                           ${user_address}
    Input Recovery Email                  ${user_address}
    Click Button                          ${BUTTON_RECOVERY}
