*** Settings ***
Documentation  Testcase for Optimy App Login page.
...
...            Author: ealap
...            MIT License. Copyright 2021 Emmanuel Alap
Default Tags   all  optimy-tests  login

Suite Setup     Setup Test
Suite Teardown  Close Browser
Resource        ../../common/keywords.resource

*** Variables ***
${BUTTON_LOGIN}           default:Login
${HELP_EMAIL}             xpath://*[@id="tab-login"]/form/div[1]/span
${HELP_PASSWORD}          xpath://*[@id="tab-login"]/form/div[2]/span
${HELP_LOGIN}             id:login-invalid
${HELP_LIMIT_REACHED}     id:login-too-many-attempts
${MESSAGE_FAILED_LOGIN}   The email address or password is incorrect.
${MESSAGE_LIMIT_REACHED}  This account has been locked due to too many failed login attempts.
${LOGIN_ATTEMPT_LIMIT}    5 times


*** Test Cases ***
Invalid Email Address Format Should Not Be Accepted
    Input Email                           not_an_email
    Set Focus To Element                  ${FIELD_PASSWORD}
    Element Should Contain                ${HELP_EMAIL}           ${MESSAGE_INVALID}

Empty Email Address And Password Should Fail
    Enter And Submit Credentials          ${EMPTY}                ${EMPTY}
    Element Should Contain                ${HELP_EMAIL}           ${MESSAGE_REQUIRED}
    Element Should Contain                ${HELP_PASSWORD}        ${MESSAGE_REQUIRED}

Empty Email Address And Valid Password Should Fail
    Enter And Submit Credentials          ${EMPTY}                ${PASS}
    Element Should Contain                ${HELP_EMAIL}           ${MESSAGE_REQUIRED}
    Element Should Not Be Visible         ${HELP_PASSWORD}

Valid Email Address And Empty Password Should Fail
    Enter And Submit Credentials          ${USER}                 ${EMPTY}
    Element Should Not Be Visible         ${HELP_EMAIL}
    Element Should Contain                ${HELP_PASSWORD}        ${MESSAGE_REQUIRED}

Unauthorized Credential Should Fail
    Enter And Submit Credentials          ${USER}                 ${PASS}
    Element Should Not Be Visible         ${HELP_EMAIL}
    Element Should Not Be Visible         ${HELP_PASSWORD}
    Wait Until Element Is Not Visible     ${LOADER_ICON}
    Element Should Contain                ${HELP_LOGIN}           ${MESSAGE_FAILED_LOGIN}

Account Should Be Locked After Too Many Fail Attempts
    Enter And Submit Credentials          ${USER}                 ${PASS}
    Wait Until Keyword Succeeds           ${LOGIN_ATTEMPT_LIMIT}  1  Reach Account Locking Limit

*** Keywords ***
Input Email
    [Arguments]                           ${user_address}
    Input Text                            ${FIELD_EMAIL}          ${user_address}

Input Password
    [Arguments]                           ${user_password}
    Input Text                            ${FIELD_PASSWORD}       ${user_password}

Enter And Submit Credentials
    [Arguments]                           ${user_address}         ${user_password}
    Input Email                           ${user_address}
    Input Password                        ${user_password}
    Click Button                          ${BUTTON_LOGIN}

Reach Account Locking Limit
    Click Button                          ${BUTTON_LOGIN}
    Wait Until Element Is Not Visible     ${LOADER_ICON}
    Element Should Contain                ${HELP_LIMIT_REACHED}   ${MESSAGE_LIMIT_REACHED}
